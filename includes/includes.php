<?php
/* %%%%%%%%%%%%%%%%%%%% MENSAJES               */
	if($mensaje!=''){
		$mensajes='
			<div class="uk-container">
				<div uk-grid>
					<div class="uk-width-1-1 margin-v-20">
						<div class="uk-alert-'.$mensajeClase.'" uk-alert>
							<a class="uk-alert-close" uk-close></a>
							'.$mensaje.'
						</div>					
					</div>
				</div>
			</div>';
	}

/* %%%%%%%%%%%%%%%%%%%% RUTAS AMIGABLES        */
		$rutaInicio			=	$ruta;
		$rutaMenu 			=	$ruta.'menu';
		$rutaNosotros       =   $ruta.'nosotros';
		$rutaSucursales		=	$ruta.'sucursales';
		$rutaContactos		=	$ruta.'contactos';

/* %%%%%%%%%%%%%%%%%%%% MENU                   */
	$menu='
		<li class="'.$nav1.' padding-20">
			<div>
				<a class="link-menu" href="'.$rutaInicio.'">INICIO</a>
			</div>
			<div class="menu-activo"></div>
		</li>
		<li class="'.$nav2.' padding-20">
			<div>
				<a class="link-menu" href="'.$rutaMenu.'">MENÚ</a>
			</div>
			<div class="menu-activo"></div>
		</li>
		<li class="'.$nav3.' padding-20">
			<div>
				<a class="link-menu" href="'.$rutaNosotros.'">NOSOTROS</a>
			</div>
			<div class="menu-activo"></div>
		</li>
		<li class="'.$nav3.' padding-20">
			<div>
				<a class="link-menu" href="'.$rutaSucursales.'">SUCURSALES</a>
			</div>
			<div class="menu-activo"></div>
		</li>
		<li class="'.$nav3.' padding-20">
		<div>
			<a class="link-menu" href="'.$rutaContactos.'">CONTACTOS</a>
		</div>
		<div class="menu-activo"></div>
		</li>
		';

	$menuMovil='
		<li class="'.$nav1.'"><a href="'.$rutaInicio.'">INICIO</a></li>
		<li class="'.$nav2.'"><a href="'.$rutaMenu.'">MENÚ</a></li>
		<li class="'.$nav3.'"><a href="'.$rutaNosotros.'">NOSOTROS</a></li>
		<li class="'.$nav3.'"><a href="'.$rutaSucursales.'">SUCURSALES</a></li>
		<li class="'.$nav3.'"><a href="'.$rutaContactos.'">CONTACTOS</a></li>
		';

/* %%%%%%%%%%%%%%%%%%%% HEADER                 */
	$header='
		<div class="uk-offcanvas-content uk-position-relative">

			<header>
				<div class="">
					<div uk-grid class="uk-grid-match">

						<!-- Botón menú móviles -->
						<div class="uk-width-auto uk-hidden@s">
							<a href="#menu-movil" uk-toggle class="uk-button uk-button-default color-primary"><i class="fa fa-bars" aria-hidden="true"></i> &nbsp; MENÚ</a>
						</div>

						<!-- Menú escritorio -->
						<div class="uk-width-1-1" style="border-bottom: 2px solid black; background: #feda00;">
							<div class="uk-margin-large-right" style="padding: 0px;">
								<div class="uk-flex uk-flex-right">
									<ul class="uk-navbar-nav">
										<li><a  href="">TEL. '.$telefono.' </a></li>
										<li>
											<a href="#" class="uk-icon-link uk-margin-small-right" style="padding: 0" uk-icon="whatsapp">
											</a>
										<li>
										<li>
											<a href="#" class="uk-icon-link uk-margin-small-right" style="padding: 0" uk-icon="facebook">
											</a>
										</li>	
										<li>
											<a href="#" class="uk-icon-link" style="padding: 0" uk-icon="instagram"></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="uk-container uk-position-relative uk-width-expand uk-visible@s" style="margin-top: 0; background: #feda00;">
							<div class="uk-width-1-1 uk-flex uk-flex-right">
								<nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
									
									<div class="uk-navbar-right" >
										<ul class="uk-navbar-nav" >

											'.$menu.'
										</ul>

									</div>
								</nav>

							</div>
							<div class="uk-position-absolute" style="top: 20px; left: 150px; width: 200px;"> 
						       <img src="img\design\logo-pastes.png" style="width: 200px; height: 200px;">
							</div>	
						</div>


					</div>
				</div>

			</header>

			'.$mensajes.'

			<!-- Menú móviles -->
			<div id="menu-movil" uk-offcanvas="mode: push;overlay: true">
				<div class="uk-offcanvas-bar uk-flex uk-flex-column">
					<button class="uk-offcanvas-close" type="button" uk-close></button>
					<ul class="uk-nav uk-nav-primary uk-nav-parent-icon uk-nav-center uk-margin-auto-vertical" uk-nav>
						'.$menuMovil.'
					</ul>
				</div>
			</div>';

/* %%%%%%%%%%%%%%%%%%%% FOOTER                 */
	//$whatsIconClass=(isset($_SESSION['whatsappHiden']))?'':'uk-hidden';
	//$stickerClass=($carroTotalProds==0 OR $identificador==500 OR $identificador==501 OR $identificador==502)?'uk-hidden':'';
	$footer = '
		<footer>
			<div class="bg-footer" style="z-index: 0;">
				<div class="uk-container uk-position-relative">
					<div class="uk-width-1-1 uk-text-center">
						<div class="padding-v-50">
							'.date('Y').' todos los derechos reservados Diseño por <a href="https://wozial.com/" target="_blank" class="color-negro">Wozial Marketing Lovers</a>
						</div>
					</div>
				</div>
			</div>
		</footer>

		

		'.$loginModal.'

		
	<div id="spinnermodal" class="uk-modal-full" uk-modal>
		<div class="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle uk-height-viewport">
			<div>
				<div class="claro" uk-spinner="ratio: 5">
				</div>
			</div>
		</div>
   	</div>';

/* %%%%%%%%%%%%%%%%%%%% HEAD GENERAL                */
	$headGNRL='
		<html lang="'.$languaje.'">
		<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">

			<meta charset="utf-8">
			<title>'.$title.'</title>
			<meta name="description" content="'.$description.'" />
			<meta property="fb:app_id" content="'.$appID.'" />
			<link rel="image_src" href="'.$ruta.$logoOg.'" />

			<meta property="og:type" content="website" />
			<meta property="og:title" content="'.$title.'" />
			<meta property="og:description" content="'.$description.'" />
			<meta property="og:url" content="'.$rutaEstaPagina.'" />
			<meta property="og:image" content="'.$ruta.$logoOg.'" />

			<meta itemprop="name" content="'.$title.'" />
			<meta itemprop="description" content="'.$description.'" />
			<meta itemprop="url" content="'.$rutaEstaPagina.'" />
			<meta itemprop="thumbnailUrl" content="'.$ruta.$logoOg.'" />
			<meta itemprop="image" content="'.$ruta.$logoOg.'" />

			<meta name="twitter:title" content="'.$title.'" />
			<meta name="twitter:description" content="'.$description.'" />
			<meta name="twitter:url" content="'.$rutaEstaPagina.'" />
			<meta name="twitter:image" content="'.$ruta.$logoOg.'" />
			<meta name="twitter:card" content="summary" />

			<meta name="viewport"       content="width=device-width, initial-scale=1">

			<link rel="icon"            href="'.$ruta.'img/design/favicon.ico" type="image/x-icon">
			<link rel="shortcut icon"   href="img/design/favicon.ico" type="image/x-icon">
			<link rel="stylesheet"      href="https://cdn.jsdelivr.net/npm/uikit@'.$uikitVersion.'/dist/css/uikit.min.css" />
			<link rel="stylesheet/less" href="css/general.less" >
			<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Lato:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
			<link href="https://fonts.googleapis.com/css2?family=Kaushan+Script&display=swap" rel="stylesheet">
			<!-- jQuery is required -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

			<!-- UIkit JS -->
			<script src="https://cdn.jsdelivr.net/npm/uikit@'.$uikitVersion.'/dist/js/uikit.min.js"></script>
			<script src="https://cdn.jsdelivr.net/npm/uikit@'.$uikitVersion.'/dist/js/uikit-icons.min.js"></script>

			<!-- Font Awesome -->
			<script src="https://kit.fontawesome.com/910783a909.js" crossorigin="anonymous"></script>

			<!-- Less -->
			<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>
		</head>';

/* %%%%%%%%%%%%%%%%%%%% SCRIPTS                */
	$scriptGNRL='
		<script src="js/general.js"></script>

		
		';

	// Script login Facebook




/* %%%%%%%%%%%%%%%%%%%% BUSQUEDA               */
	$scriptGNRL.='
		<script>
			$(document).ready(function(){
				$(".search").keyup(function(e){
					if(e.which==13){
						var consulta=$(this).val();
						var l = consulta.length;
						if(l>2){
							window.location = ("'.$ruta.'"+consulta+"_gdl");
						}else{
							UIkit.notification.closeAll();
							UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
						}
					}
				});
				$(".search-button").click(function(){
					var consulta=$(".search-bar-input").val();
					var l = consulta.length;
					if(l>2){
						window.location = ("'.$ruta.'"+consulta+"_gdl");
					}else{
						UIkit.notification.closeAll();
						UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
					}
				});
			});
		</script>';





<!DOCTYPE html>
<?=$headGNRL?>
<body style="background-image: url(img/contenido/varios/fondo.png);">
  
<?=$header?>

<div class="bg-primary">
	<div class="uk-container padding-bottom-5 padding-top-50" style="max-width: 900px; ">
		<?=carousel('carousel')?>
	</div>
</div>
<div style="margin-top: -5px;">
	<img src="img/contenido/carousel/barraInf.png" alt="">
</div>

<!-- Productos de muestra -->
 <div>
     <div class="uk-container padding-v-50" style="visibility:hidden;" uk-scrollspy="cls:uk-animation-fade;delay:500;">
        <h2 class="uk-text-center" style="  font-weight: 600;">Pastes</h2>
        <div class="uk-flex uk-flex-center">
            <div class="uk-text-center">
				<ul uk-tab class="uk-flex uk-flex-center">
					<?php 	
					$sqlCat ="SELECT * FROM productoscat ORDER BY id";
					$ConsultaCat= $CONEXION -> query($sqlCat);
					while ($row_ConsultaCat = $ConsultaCat -> fetch_assoc()) {
        				echo '<li><a href="#">'.$row_ConsultaCat['txt'].' </a></li>';
    				}
					?>
				</ul>

				<ul class="uk-switcher uk-margin">
			<?php
			$sqlCat ="SELECT * FROM productoscat ORDER BY id";
			$ConsultaCat= $CONEXION -> query($sqlCat);
			while ($row_ConsultaCat = $ConsultaCat -> fetch_assoc()) {
				$idCat = $row_ConsultaCat['id'];
				echo '




					<li>
						<div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slider="sets:true; autoplay:true; autoplay-interval:3000;">
	  					
	  						<ul class="uk-slider-items uk-child-width-1-2@s uk-text-center" uk-height-match="target: .texto">';
	     							$ConsultaCat2= $CONEXION -> query("SELECT * FROM productos WHERE categoria = $idCat");
								while ($row_ConsultaCat2 = $ConsultaCat2 -> fetch_assoc()) {
	        						echo '<li>

	        						<div class="uk-child-width-1-2@s uk-grid-match" uk-grid>
					<div style="margin: 10px;">'.item($row_ConsultaCat2['id']).'</div>

	        						</li>';
								}
	      
	  						    echo '
	   						</ul>

						    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin uk-visible@m"></ul>
	    					<a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
	    					<a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

  						</div>

 	 				</li>';

			} 	
	?>
				</ul>

			</div>
		</div>
	</div>


	<div class="uk-grid-collapse uk-child-width-expand@l uk-text-center uk-margin-large-top" uk-grid>
		<div>
			<div class="uk-cover-container" style="height: 300px;">
				<img src="img\contenido\productos\pas.jpg" uk-cover>
				<div class="uk-flex uk-flex-center">
                	<div class="uk-position-top uk-position-medium uk-text-center">
						<h2 class="title-small">	                              
                        	Pack Paste
                        </h2>
                        <div class="uk-position-medium uk-text-center">
                        	<p class="texto-descripcion">Llévate este pack de pastes lleno de sabor</p>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<div>
		<div class="uk-cover-container" style="height: 300px;">
			<img src="img\contenido\productos\paste.jpg" uk-cover>
			<div class="uk-flex uk-flex-center">
                	<div class="uk-position-top uk-position-medium uk-text-center">
						<h2 class="title-small">	                              
                        	Especial
                        </h2>
                        <div class="uk-position-medium uk-text-center">
                        	<p class="texto-descripcion">Disfruta de nuestro paste especial de temporada</p>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		</div>  

		<div style="background: #feda00;" class="uk-grid-collapse uk-child-width-expand@l uk-text-center ">
		
			<div  class="uk-cover-container" style="height: 600px;">
				<div class="uk-grid-small uk-text-center uk-margin-large-top">

					<h2 style="font-size: 50px; color: black; font-weight: 700;">Nosotros</h2>
					
					<div class="uk-flex uk-flex-center">
    <div class="uk-child-width-1-1 uk-card" style="max-width: 650px; color: black;"><?= $about ?></div>
    
</div>
					<div class="uk-flex uk-flex-center">
					<div class="uk-grid-small uk-child-width-expand@s uk-text-center" uk-grid>
    					<div>

        					<div class="uk-padding" ><br><img width="150" height="150" src="img\contenido\galerias\icono1.png">
        						<p style=" color: black;">Tenemos diversas sucursales en guadalajara</p>
        					</div>
    						</div>
    						<div>
        						<div class="uk-padding"><img width="150" height="150" src="img\contenido\galerias\icono2.png">
        							<p style=" color: black;">excelente presentación.</p>
        						</div>
    							</div>
    							<div>
        							<div class=" uk-padding"><br><img width="150" height="150" src="img\contenido\galerias\icono3.png">
									<p style=" color: black;">Tenemos el mejor sabor.</p>
        							</div>
    								</div>
								</div>
								</div>
					
			</div>
		</div> 
	
</div>
<div class="uk-margin-large-top uk-container" style="max-width: 1700px;">
<div uk-slider="">
    <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1">

        <ul class="uk-slider-items uk-child-width-1-5@s uk-grid">
           
                	<?php 
$sql="SELECT * FROM sucursales ORDER BY id";
$consultaSucursales = $CONEXION-> query($sql);

while ($row_ConsultaSuc = $consultaSucursales -> fetch_assoc()) {
	echo '
			 <li>
                <div class="text-9 uk-text-center uk-card " style=" max-height:500px; width: 250px; border-bottom: 3px solid; border-bottom-color: #feda00;">
              		<div  class="circle-card uk-card-media-top" style="background: #feda00;">
                    	<div class="uk-position-medium" style="height: 200px; ">
                        <img class="uk-position-small" src="img/contenido/sucursales/ubicacion.png" alt="">
                   		 </div>
                  	</div>
                    <div style="height: 100px;">
                        <p>'.$row_ConsultaSuc['txt'].'</p>
                        <p>'.$row_ConsultaSuc['titulo'].'</p>
                    </div>
                    
                </div>
            </li>
	';
}
                	?>            
        </ul>

        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

    </div>

    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>

</div>
</div>

<div class="uk-grid-collapse" uk-grid>
	<div class="uk-width-expand uk-flex uk-flex-middle">
        	<div class="line"></div>
	</div>
	<div class="uk-width-auto">
		<div class="contacto-titulo">
        Contacto	
        </div>
	</div>
	<div class="uk-width-expand uk-flex uk-flex-middle">
        	<div class="line"></div>
	</div>
</div>  

<!-- Formulario prueba -->
	<div class="uk-container uk-container-small" style="min-height: 80vh; max-width: 700px;">
		<div class="uk-width-1-1 uk-card uk-card-body" style="padding-left: 1px;">
			<div uk-grid class="uk-child-width-1-3@s" style="padding-left: 1px;">
				<div>
					<input type="text" class="uk-input input-personal" id="footernombre" placeholder="Nombre">
				</div>
				<div>
					<input type="email" class="uk-input input-personal" id="footeremail" placeholder="Correo">
				</div>
				<div>
					<input type="text" class="uk-input input-personal" id="footertelefono" placeholder="Teléfono">
				</div>
			</div>
			<div class="margin-top-20">
				<div>
					<textarea type="text" class="uk-textarea input-personal" id="footercomentarios" placeholder="Mensaje"></textarea>
				</div>
			</div>
			<div class="uk-flex uk-flex-center">
				<div class="button-dark margin-top-10 uk-text-center">
					<button class="circle-dark uk-button uk-button-personal footer-enviar" id="footersend">Enviar</button>
				</div>
			</div>

		</div>

<div class="uk-grid-small uk-child-width-expand@s uk-text-center" uk-grid>
    <div>
        <div style="font-size: 50px; color: black;" class="uk-card uk-card-body" >ENCUÉNTRANOS EN:</div>
    </div>
</div>
<div class="uk-grid-small uk-child-width-expand@s uk-text-center" uk-grid>
    <div>
        <a href="uk-button"> <div class="circle-app uk-card uk-card-default uk-card-body" style="padding: 0px;"> <img class="circle-app" src="img/design/rappi.png" alt="rappi"> </div> </a>
    </div>
    <div>
        <a href="uk-button"> <div class="circle-app uk-card uk-card-default uk-card-body" style="padding: 0px;"><img class="circle-app" src="img/design/uber-eats.png" alt="uber"> </div> </a>
    </div>
    <div>
        <a href="uk-button"> <div class=" circle-app uk-card uk-card-default uk-card-body" style="padding: 0px;"><img class="circle-app" src="img/design/sin-delantal.png" alt="sin-delantal"> </div> </a>
    </div>
    <div>
        <a href="uk-button"> <div class="circle-app uk-card uk-card-default uk-card-body" style="padding: 0px;"><img class="circle-app" src="img/design/didi.png" alt="didi"> </div> </a>
    </div>
</div>
	</div><!-- Formulario -->






        <div class="uk-padding-0">
<img src="img/contenido/carousel/barraInv.png" style="margin-bottom: -11px!important; width: 7000px;">
        </div>
 



    
        <div class=" uk-padding-0" style="background-color: #feda00;">
        	<div class="uk-container uk-position-relative uk-width-expand uk-visible@s" style="margin-top: 0; background: #feda00;">
							<div class="uk-width-1-1 uk-flex uk-flex-center" style>
								<nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
									
									<div class="uk-navbar-right">
										<ul class="uk-navbar-nav">

											<?= $menu ?>
										</ul>

									</div>
								</nav>

							</div>
        </div>
    </div>
    <div>
        <div class=" uk-padding-0" style="background-color: #feda00;">Item</div>
    </div>
    <div class=" uk-padding-0" style="background-color: #feda00;">Item</div>
    </div>
</div>
</body>
</html>